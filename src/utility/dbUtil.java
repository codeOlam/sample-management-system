package utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author CodeOlam
 */
public class dbUtil {
    private Connection con;
    private Statement st;
    private PreparedStatement ps;
    
    //******** db credentials ***************
    private final String url = "jdbc:postgresql://localhost:5432/pg_sample_db";
    private final String user = "username";     //Replace with your postgres credentials
    private final String password = "password";     //Replace with your postgres credentials
    
    
    
    //************************ Function dbConnection ****************************
    public Connection dbConnection() {
        /**This function creates connection with the data base
        */
        
        try {
            
            con = DriverManager.getConnection(url, user, password);
            
            System.out.println("Connection Successfull!");
            
            return con;
        } catch (SQLException ex) {
            
            System.out.println("Connection Failed!");
            return null;
        }
    } //End function dbConnection()
    
    
    
    
    //********************* create db table ************************************
    public void create_staff_table() {
        /*This function will create a new database table if it is not exit otherwise
        it will use the exist table and skip the function logic */
        
        try {
                con = dbConnection();


                st = con.createStatement();


            //String for table headings and data structure
            String sql ="CREATE TABLE IF NOT EXISTS staff"
                        +"(staff_id serial primary key not null,"
                    + "firstname varchar(50) not null,"
                    + "lastname varchar(50) not null,"
                    + "email varchar(50) not null,"
                    + "createdAt timestamp )";

            st.executeUpdate(sql);
            st.close();

           System.out.println("Table create succefully!");

            } catch (SQLException ex) {
                Logger.getLogger(dbUtil.class.getName()).log(Level.SEVERE, null, ex);


                System.out.println("\nTable Could not be Created!");

            }

    }//End function creat_staff_table()
    
    

    //*********************This function will create static data to populate the Staff table *****************
    public void insertData(){
        /**This function will insert static data in to the data table created*/
        try {
            // calls function from dbUtil to create the staff table
            create_staff_table();
            
            
            String first_name       = "Frank";
            String last_name        = "Uchenna";
            String email_           = "frankuchenna@gmail.com";
            LocalDateTime time_stp  = LocalDateTime.now();
            
            String sql = "INSERT INTO staff(firstname, lastname, email, createdAt) VALUES (?, ?, ?, ?)";
            
            ps = con.prepareStatement(sql);
            

            ps.setString(1, first_name);
            ps.setString(2, last_name);
            ps.setString(3, email_);
            ps.setObject(4, time_stp);
            
            ps.executeUpdate();

            //con_db.close();
            
            System.out.println("\nData Successfully added to db Table");
            
            
        } catch (SQLException ex) {
            Logger.getLogger(dbUtil.class.getName()).log(Level.SEVERE, null, ex);
           System.out.println("\nFailed to add Data to db Table!");
        }

    }// end function inserData()

}
