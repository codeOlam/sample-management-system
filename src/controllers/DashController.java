/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import models.StockModel;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.PieChart;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author PrisonerOfChrist
 */
public class DashController implements Initializable {

    @FXML
    private TableView<StockModel> dash_table;
    @FXML
    private TableColumn<StockModel, Integer> col_dash_id;
    @FXML
    private TableColumn<StockModel, String> col_nokia;
    @FXML
    private TableColumn<StockModel, String> col_samsung;
    @FXML
    private TableColumn<StockModel, String> col_iphone;
    @FXML
    private TableColumn<StockModel, String> col_others;
    @FXML
    private PieChart pieChart;

    
    
        //Handles loading of piechart data
    @FXML
    private void loadPieChartData(){
        
        PieChart.Data slice1 = new PieChart.Data("Nokia 5S", 13);
        PieChart.Data slice2 = new PieChart.Data("Samsung", 25);
        PieChart.Data slice3 = new PieChart.Data("Iphone", 10);
        PieChart.Data slice4 = new PieChart.Data("Others", 22);

        pieChart.getData().add(slice1);
        pieChart.getData().add(slice2);
        pieChart.getData().add(slice3);
        pieChart.getData().add(slice4);
           
        
    }
    
    // Generating table data

    private void loadDashTable(){
         ObservableList<StockModel> stockModel = FXCollections.observableArrayList(
                 
         new StockModel(1, "Nokia", "Samsung", "Iphone", "Others"),
         new StockModel(2,"Nokia", "Samsung", "Iphone", "Others"),
         new StockModel(3,"Nokia", "Samsung", "Iphone", "Others"),
         new StockModel(4,"Nokia", "Samsung", "Iphone", "Others"));
         
        col_dash_id.setCellValueFactory(new PropertyValueFactory<>("id"));
        col_nokia.setCellValueFactory(new PropertyValueFactory<>("nokia"));
        col_samsung.setCellValueFactory(new PropertyValueFactory<>("samsung"));
        col_iphone.setCellValueFactory(new PropertyValueFactory<>("iphone"));
        col_others.setCellValueFactory(new PropertyValueFactory<>("others"));
        dash_table.setItems(stockModel);
    }
    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        loadPieChartData();
        loadDashTable();
    }    
    
}
