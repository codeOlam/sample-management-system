package controllers;

import utility.dbUtil;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javax.swing.text.Document;


/**
 * FXML Controller class
 *
 * @author codeOlam
 */
public class StaffController implements Initializable {
    
    //******************* init of db parameters ******************
    private dbUtil db;
    private Connection con_db;
    private PreparedStatement ps;

    
    //************* init observableArray *******************
    private ObservableList<StaffModel> data;    
    
    
    
    //For Staff Page
    @FXML
    private TableView<StaffModel> table_staff;
    @FXML
    private TableColumn<StaffModel, Integer> staff_id;
    @FXML
    private TableColumn<StaffModel, String> staff_fname;
    @FXML
    private TableColumn<StaffModel, String> staff_lname;
    @FXML
    private TableColumn<StaffModel, String> staff_email;
    @FXML
    private TableColumn<StaffModel, String> staff_createdAt;
    @FXML
    private TextField search_staff;
    @FXML
    private Button btn_search;
    @FXML
    private FontAwesomeIconView btn_search_staff;
    
    
    @FXML
    private TextField id_txtbox;

    @FXML
    private TextField fn_txtbox;

    @FXML
    private TextField ln_txtbox;

    @FXML
    private TextField email_txtbox;

    @FXML
    private Button btn_add;

    @FXML
    private Button btn_edit;

    @FXML
    private Button btn_update;
    
    @FXML
    private Button btn_remove;
    
    @FXML
    private Button btn_clear;
    
    
    private boolean EDIT=false, ADD=false;
    
    private int ID;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        db = new dbUtil();
        
        con_db = db.dbConnection();

        
    }    
    
    
    
/*
    @FXML
    private void handleClicks(ActionEvent event) {
        
        if(event.getSource() == btn_add_staff){
            
            try {
                Scene scene = new Scene(FXMLLoader.load(getClass().getClassLoader().getResource("fxml/Add_staff.fxml")));
                
                Stage stage_add_staff = new Stage();
                stage_add_staff.setScene(scene);
                // To set a static stage
                stage_add_staff.setResizable(false);
                stage_add_staff.show();
            } catch (IOException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                ex.getMessage();
            }
        }
        else if(event.getSource() == btn_add_s){
            insertData();
            clearField();
        }
        
        else if(event.getSource() == btn_cancle){
            Stage add_stage_cancle = (Stage) btn_cancle.getScene().getWindow();
            add_stage_cancle.close();
        }
    }// End handleClicks function
    
  */  
    
    
    
    //*********************This function will create static data to populate the Staff table *****************
    public void insertData(){
        /**This function will insert static data in to the data table created*/
        try {
            // calls function from dbUtil to create the staff table
            db.create_staff_table();
            
            
            String first_name       = fn_txtbox.getText();
            String last_name        = ln_txtbox.getText();
            String email_           = email_txtbox.getText();
            LocalDateTime time_stp  = LocalDateTime.now();
            
            String sql = "INSERT INTO staff(firstname, lastname, email, createdAt) VALUES (?, ?, ?, ?)";
            
            ps = con_db.prepareStatement(sql);
            

            ps.setString(1, first_name);
            ps.setString(2, last_name);
            ps.setString(3, email_);
            ps.setObject(4, time_stp);
            
            ps.executeUpdate();


            
            System.out.println("\nData Successfully added to db Table");
            
            
        } catch (SQLException ex) {
            Logger.getLogger(dbUtil.class.getName()).log(Level.SEVERE, null, ex);
           System.out.println("\nFailed to add Data to db Table!");
        }

    }// end function inserData()
    
    
    
   //******************** Function to load *************************
    public void loadStaffTable(){
        try {
            /*This function load data from the database
            */
            
            // creating the observablelist array object
            data = FXCollections.observableArrayList();
            
            //Query database table
            String query = "SELECT * FROM staff";
            // Execute query and store result in resultset
            ResultSet rs = con_db.createStatement().executeQuery(query);
            
            //Loop throw result set and populate table clumns and rows with values from rs.
            while(rs.next()){
                data.add(new StaffModel(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4), 
                        rs.getString(5)));
                
            }
            
            System.out.println("The value of data: \n"+ data);
            
            
        } catch (SQLException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            System.out.print("Could not load db");
        }
        
        
        //Added data to UI table colmns and rows.
        staff_id.setCellValueFactory(new PropertyValueFactory<>("staffId"));
        staff_fname.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        staff_lname.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        staff_email.setCellValueFactory(new PropertyValueFactory<>("email"));
        staff_createdAt.setCellValueFactory(new PropertyValueFactory<>("createdAt"));
        
        table_staff.setItems(null);
        table_staff.setItems(data);  

    } //End function loadStaffTable

  
    
    
    //*************** The Event handling functions for CRUD *************
    
    public void handleAdd(ActionEvent event){
        if (event.getSource() == btn_add){
            insertData();
            clearField();
            loadStaffTable();
        }
    } //End Add Function Handle.
    
    
    
    public void handleEdit(ActionEvent event){
        if (event.getSource() == btn_edit){
            ADD=false;
            EDIT=true;
            
            editAccount();
        }
        
    }// End edit function handle.
    
    
    
    public void handleUpdate(ActionEvent event) throws SQLException{
        if (event.getSource() == btn_update){
            updateAccount();
            
            clearField();
        }
        
    }// End Update function handle.
    
    
    public void handleClear(ActionEvent event){
        if (event.getSource() == btn_clear){
            clearField();
        }
    } // End Clear Function handle
    
    
    public void handleRemove(ActionEvent event) throws SQLException{
        if (event.getSource() == btn_remove){
            deleteAccount();
        }
    } // End Remove Function Handle.
    
    
    
    //**************** Search Query function *************************
    public void handleSearch(ActionEvent event) throws SQLException{
        if (event.getSource() == btn_search){
            searchItem();
            System.out.println("The search button is being clicked");

        }
    }//End function search query
    
    
    //***************** The CRUD functions ****************************
    
    
    
    
    //********************************* clearing text filed *******************
    @FXML
    private void clearField(){
        id_txtbox.clear();
        fn_txtbox.clear();
        ln_txtbox.clear();
        email_txtbox.clear();
        
    } // End funtion clearField
    
    
    //***************** The Edit function ****************************
    
    @FXML
    private void editAccount(){
        StaffModel selected = table_staff.getSelectionModel().getSelectedItem();
        ID = selected.getStaffId();
 
        id_txtbox.setText(Integer.toString(ID));
        fn_txtbox.setText(selected.getFirstName());
        ln_txtbox.setText(selected.getLastName());
        email_txtbox.setText(selected.getEmail());
        
    }// end function editAccout
    
    
    //***************** The Save Function ****************************
    
    @FXML
    private void updateAccount() throws SQLException{
        if(EDIT){
            String upatequery = "UPDATE staff SET firstname = ?, lastname = ?, email = ? WHERE staff_id = ?";
            
            ps = con_db.prepareStatement(upatequery);
            

            ps.setString(1, fn_txtbox.getText());
            ps.setString(2, ln_txtbox.getText());
            ps.setString(3, email_txtbox.getText());
            ps.setInt(4, Integer.parseInt(id_txtbox.getText()));
 
            
            ps.executeUpdate();
            loadStaffTable();
            
        }
    } // End function updateAccount()
    
    
    
    //*************** The Delete Function *****************************
    
    @FXML
    private void deleteAccount() throws SQLException{
        StaffModel selected = table_staff.getSelectionModel().getSelectedItem();
        ID = selected.getStaffId();

        
        String deletequery = "DELETE FROM staff WHERE staff_id = "+ID+"";
        ps = con_db.prepareStatement(deletequery);


        ps.executeUpdate();
        loadStaffTable();
    } //End function delectAccount
    
    
    //************************** THe search function ********************
    @FXML
    private void searchItem() throws SQLException{
        
        String value_ = search_staff.getText();

        String search_query = "SELECT * FROM staff WHERE firstname = ?";
        
        ps = con_db.prepareStatement(search_query);
        ps.setString(1, value_);
        
        ResultSet rs = ps.executeQuery();
            
            //Clear observeableList 
            data.removeAll(data);
            
            System.out.println("Cleared data: "+ data);
            //Loop throw result set and populate table clumns and rows with values from rs.
            while(rs.next()){
                data.add(new StaffModel(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4), 
                        rs.getString(5)));
                
            }
        
            System.out.println("The value of Search query: \n"+ data);
        
        //Added data to UI table colmns and rows.
        staff_id.setCellValueFactory(new PropertyValueFactory<>("staffId"));
        staff_fname.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        staff_lname.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        staff_email.setCellValueFactory(new PropertyValueFactory<>("email"));
        staff_createdAt.setCellValueFactory(new PropertyValueFactory<>("createdAt"));

         
        table_staff.setItems(null);
        table_staff.setItems(data);            
        
    }// End search function
    
    
    //******************** Table to Pdf converter function *****************************
    //This function is yet to be completed.
    @FXML
    private void Convert_toPDF() throws SQLException{
        String db_query = "SELECT * FROM staff";
        
        ResultSet rs = con_db.createStatement().executeQuery(db_query);
        
    
    }//End function Convert_toPDF()
}
