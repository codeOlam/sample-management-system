
package controllers;

import java.sql.Timestamp;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author PrisonerOfChrist
 */
public class StaffModel {

    private final IntegerProperty staff_id;
    private final StringProperty first_name;
    private final StringProperty last_name;
    private final StringProperty email;
    private final SimpleObjectProperty<Timestamp> created_at;
    
    //Constructor
    public StaffModel(int staffId, String firstName, String lastName, String email, String createdAt ){
        this.staff_id = new SimpleIntegerProperty(staffId);
        this.first_name = new SimpleStringProperty(firstName);
        this.last_name = new SimpleStringProperty(lastName);
        this.email = new SimpleStringProperty(email);
        this.created_at = new SimpleObjectProperty(createdAt);
    }

    
    // staff_id
    public int getStaffId(){
        return staff_id.get();
    }
    
    public void setStaffId(int staffId){
        this.staff_id.set(staffId);
    }
    
    public IntegerProperty staffIdProperty(){
        return staff_id;
    }
    
    // first_name
    public String getFirstName(){
        return first_name.get();
    }
    
    public void setFirstName(String firstName){
        this.first_name.set(firstName);
    }
    
    public StringProperty firstNameProperty(){
        return first_name;
    }
    
    //last_name
    public String getLastName(){
        return last_name.get();
    }
    
    public void setLastName(String lastName){
        this.last_name.set(lastName);
    }
    
    public StringProperty lastNameProperty(){
        return last_name;
    }
    
    //email 
    public String getEmail(){
        return email.get();
    }
    
    public void setEmail(String email){
        this.email.set(email);
    }
    
    public StringProperty emailProperty(){
        return email;
    }
    
    //createdAt
    public Object getCreatedAt(){
        return created_at.get();
    }
    
    public void setCreatedAt (Timestamp createdAt){
        this.created_at.set(createdAt);
    }
    
    public SimpleObjectProperty<Timestamp> createdAtProperty(){
        return created_at;
    }


}
    
