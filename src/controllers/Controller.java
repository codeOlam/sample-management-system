/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;



import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 *
 * @author PrisonerOfChrist
 */
public class Controller implements Initializable {
    
    //********** other init ************
    //@FXML private StaffControl staffcontrol;
    //private StaffController staffcontroller;
    
    
    //********************** For Home Page ************************
    @FXML
    private Button btn_admin;

    @FXML
    private Button btn_staff;

    @FXML
    private Button btn_cal;

    @FXML
    private Button btn_dept;

    @FXML
    private Button btn_dash;

    @FXML
    private Button btn_settings;
    
       
  


    //======================================= Fxml function below ===================================================//
    

    
    //This methods will load windows when a botton is pressed
    @FXML
    private void loadStage(String fxml){
        try {
            
            Scene scene = new Scene(FXMLLoader.load(getClass().getClassLoader().getResource(fxml)));
            
            Stage stage = new Stage();
            stage.setScene(scene);
            // To set a static stage, do the following
            //stage.setResizable(false);
            stage.show();
            

            
        } catch (IOException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            ex.getMessage();
        }

    }
    
    
    // This function handles the button pressed event
    @FXML
    private void handleButtonCliks(ActionEvent event){

        Node node = (Node) event.getSource();
        Stage stage = (Stage)node.getScene().getWindow();

        if (event.getSource() == btn_staff){
            try {
                //loadStage("fxml/Staff.fxml");
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/Staff.fxml"));
                Parent staffScene = (Parent)loader.load();
                StaffController staffcontroller = loader.getController();
                
                staffcontroller.loadStaffTable();
                
                Stage staffStage = new Stage();
                staffStage.setScene(new Scene(staffScene));
                
                staffStage.show();
            } catch (IOException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else if (event.getSource() == btn_dash){
            loadStage("fxml/DashBoard.fxml");
        }
        else if (event.getSource() == btn_cal) {
            loadStage("fxml/Calendar.fxml");
        } else {}
        
    
    }
    
    
    
    
//================================== Main Function Below ====================================== //

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
      
    }    
    
}
