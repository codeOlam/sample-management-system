package models;


import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author PrisonerOfChrist
 */
public class StockModel {
    private final IntegerProperty id;
    private final StringProperty nokia;
    private final StringProperty samsung;
    private final StringProperty iphone;
    private final StringProperty others; 
    
    //Constructor
    public StockModel(int id_, String nokia_, String samsung_, String iphone_, String others_){
        this.id = new SimpleIntegerProperty(id_);
        this.nokia = new SimpleStringProperty(nokia_);
        this.samsung = new SimpleStringProperty(samsung_);
        this.iphone = new SimpleStringProperty(iphone_);
        this.others = new SimpleStringProperty(others_);
    }



    // the following functions will retrive values from the data base
    public int getId(){
        return id.get();
    }
    
    public String getName(){
        return nokia.get();
    }
    
    public String getlName(){
        return samsung.get();
    }
    
    public String getEmail(){
        return iphone.get();
    }
    
    public String getothers(){
        return others.get();
    }
    
    
    //The following functions will set values to the database
    public void setId(int value){
        id.set(value);
    }
    
    public void setNokia(String value){
        nokia.set(value);
    }
    
    public void setSamsung(String value){
        samsung.set(value);
    }
    
    public void setIphone(String value){
        iphone.set(value);
    }
    
    public void setOthers(String value){
        others.set(value);
    }
    
    
    //Property Values
    public IntegerProperty idProperty(){
        return id; 
    }
    
    public StringProperty nokiaProperty(){
        return nokia;
    }
    
    public StringProperty samsungProperty(){
        return samsung;
    }
    
    public StringProperty iphoneProperty(){
        return iphone;
    }
    
    public StringProperty othersProperty(){
        return others;
    }

    
}
