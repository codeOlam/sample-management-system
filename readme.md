## Simple PostgreSQL implementation on JAVAFxml.

![home](screenShots/home_page.png?raw=true "home")



![dash](screenShots/dash.png?raw=true "dash")



![staff_pg](screenShots/staff_pg2.png?raw=true "staff")




## About
This is a simple javafxml application designed to be a basic template for any management system you wish to build 
as it works with the basic concept of CRUD. 
This application implements postgresql using jdbc API for database, with the following features 

- Create postgres db table 
- inserts data into db table
- retrieves data from the db table
- Edit update and delete data

The application also has the UI designed with javafx and features the following:

- A Dashboard
- A Staff Table
- Calender page for scheduling events


## Technology and Requirements
1. Java
2. JavaFx
3. Postgresql
4. Netbeans
5. SceneBuilder



## Installations
1. [NetBeans](https://netbeans.org/community/releases/82/install.html)
2. [SceneBuilder](https://docs.oracle.com/javase/8/scene-builder-2/installation-guide/jfxsb-installation_2_0.htm)
3. [PostgreSql](http://www.techken.in/how-to/install-postgresql-10-windows-10-linux/)


## Resources
1. [FontawesomeFX 8.9](https://jar-download.com/artifacts/de.jensd/fontawesomefx/8.9/source-code)
2. [Postgresql jar lib](https://jar-download.com/artifacts/org.postgresql/postgresql/42.2.8.jre6/source-code)
3. [CalenderFx](https://jar-download.com/artifacts/com.calendarfx)
4. [TutorialPoint](https://www.tutorialspoint.com/postgresql/postgresql_java.htm)
5. [DateTime](https://jdbc.postgresql.org/documentation/head/8-date-time.html)

## Other Resources
- [SceneBuilder](https://gluonhq.com/products/scene-builder/)
- [java.time package](https://docs.oracle.com/javase/8/docs/api/java/time/package-summary.html)
